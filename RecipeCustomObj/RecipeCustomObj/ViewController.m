//
//  ViewController.m
//  RecipeCustomObj
//
//  Created by Adam Farrell on 6/8/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    ListViewController* leftVC = [ListViewController new];
    RecipeViewController* rightVC = [RecipeViewController new];
    
    NSArray* tabViewControllers = @[leftVC, rightVC];
    [self setViewControllers:tabViewControllers];
    
    leftVC.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"Recipe List" image:nil tag:0];
    rightVC.tabBarItem = [[UITabBarItem alloc]initWithTitle:@"Recipe" image:nil tag:1];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
