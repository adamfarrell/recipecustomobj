//
//  Recipe.h
//  RecipeCustomObj
//
//  Created by Adam Farrell on 6/8/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Recipe : NSObject

@property (nonatomic, strong) NSString* recipeTitle;
@property (nonatomic, strong) NSString* recipeImage;
@property (nonatomic, strong) NSString* recipeInstructions;

@end
