//
//  ListViewController.m
//  RecipeCustomObj
//
//  Created by Adam Farrell on 6/8/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import "ListViewController.h"

@interface ListViewController ()

@end

@implementation ListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor primaryBackgroundColor];
    float width = self.view.bounds.size.width;
    
    float btnHeight = 40.0f;
    float btnOffset = 20.0f;
    
    NSString* filePath = [[NSBundle mainBundle]pathForResource:@"RecipePropertyList" ofType:@"plist"];
    NSArray* arrayOfDictionaries = [[NSArray alloc]initWithContentsOfFile:filePath];
    recipeList = [[NSMutableDictionary alloc]init];
    
    for (int i = 0; i < arrayOfDictionaries.count; i++) {
        Recipe* recipe = [[Recipe alloc]init];
        recipe.recipeTitle = [[arrayOfDictionaries objectAtIndex:i] objectForKey:@"title"];
        recipe.recipeImage = [[arrayOfDictionaries objectAtIndex:i] objectForKey:@"image"];
        recipe.recipeInstructions = [[arrayOfDictionaries objectAtIndex:i] objectForKey:@"instructions"];
        recipeList[recipe.recipeTitle] = recipe;
        
        CustomButton* btn = [[CustomButton alloc]initWithFrame:CGRectMake(btnOffset, 40 + ((btnHeight + 10) * i), width - btnOffset * 2, btnHeight)];
        [btn setTitle:recipe.recipeTitle forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(btnTouched:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btn];
    }
    
    
}

-(void)btnTouched:(id)sender {
    [[NSNotificationCenter defaultCenter]postNotificationName:@"recipeSelected" object:[recipeList objectForKey:[sender currentTitle]]];
    NSLog(@"%@", @"RecipeSelected");
    NSLog(@"%@", [sender currentTitle]);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
