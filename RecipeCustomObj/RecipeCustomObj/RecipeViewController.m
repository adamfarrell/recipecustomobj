//
//  RecipeViewController.m
//  RecipeCustomObj
//
//  Created by Adam Farrell on 6/8/15.
//  Copyright (c) 2015 Adam Farrell. All rights reserved.
//

#import "RecipeViewController.h"

@interface RecipeViewController ()

@end

@implementation RecipeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    float width = self.view.bounds.size.width;
    float height = self.view.bounds.size.height;
    float offset = 20.0f;
    
    self.view.backgroundColor = [UIColor primaryBackgroundColor];
    
    UIImageView* image = [[UIImageView alloc]initWithFrame:CGRectMake(offset, 40, width - (offset * 2), height / 4)];
    [image setContentMode:UIViewContentModeScaleAspectFit];
    [image setImage:[UIImage imageNamed:nil]];
    image.tag = 101;
    [self.view addSubview:image];
    
    
    UILabel* titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(offset / 2, (height / 4) + 55, width - offset, 40)];
    titleLabel.text = @"This Will Be A Recipe";
    [titleLabel setFont:[UIFont primaryTitleFont]];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor primaryFontColor];
    titleLabel.numberOfLines = 1;
    titleLabel.adjustsFontSizeToFitWidth = YES;
    titleLabel.tag = 102;
    [self.view addSubview:titleLabel];
    
    UITextView* description = [[UITextView alloc]initWithFrame:CGRectMake(offset, (height / 4) + 105, width - (offset * 2), height / 2)];
    [description setText:@"This Will be a set of instructions. This Will be a set of instructions. This Will be a set of instructions. This Will be a set of instructions. This Will be a set of instructions. This Will be a set of instructions."];
    [description setTextColor:[UIColor primaryFontColor]];
    [description setBackgroundColor:[UIColor primaryBackgroundColor]];
    [description setFont:[UIFont systemFontOfSize:16]];
    description.tag = 103;
    [self.view addSubview:description];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(notificationRecieved:) name:@"recipeSelected" object:nil];
}

-(void)notificationRecieved:(NSNotification*)notification {
    NSLog(@"%@", notification.object);
    selectedRecipe = (Recipe*)notification.object;

    
    [(UIImageView*)[self.view viewWithTag:101] setImage:[UIImage imageNamed:selectedRecipe.recipeImage]];
    
    //UIImageView* tempImg = (UIImageView*)[self.view viewWithTag:101];
    //[tempImg setImage:[UIImage imageNamed:selectedRecipe.recipeImage]];
    UILabel* tempLabel = (UILabel*)[self.view viewWithTag:102];
    tempLabel.text = selectedRecipe.recipeTitle;
    UITextView* tempDescription = (UITextView*)[self.view viewWithTag:103];
    [tempDescription setText:[selectedRecipe.recipeInstructions stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
